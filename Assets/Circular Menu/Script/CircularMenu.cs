﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircularMenu : MonoBehaviour
{
    public List<MenuButton> menuButtons = new List<MenuButton>();
    private Vector2 mousePosition;
    private Vector2 fromVector2MP = new Vector2(0.5f, 1.0f);
    private Vector2 centerCircle = new Vector2(0.5f, 0.5f);
    private Vector2 toVector2MP;

    public int menuItems;
    public int curMenuItem;
    private int preMenuItem;


	// Use this for initialization
	void Start ()
    {
        menuItems = menuButtons.Count;
        foreach(MenuButton mb in menuButtons)
        {
            mb.sceneImage.color = mb.normalColor;
        }

        curMenuItem = 0;
        preMenuItem = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetCurrentMenuItem();
        if (Input.GetButtonDown("Fire1"))
        {
            ButtonAction();
        }
	}

    public void GetCurrentMenuItem()
    {
        mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        toVector2MP = new Vector2(mousePosition.x / Screen.width, mousePosition.y / Screen.height);

        float angle = (Mathf.Atan2(fromVector2MP.y - centerCircle.y, fromVector2MP.x - centerCircle.x)
                        -
                       Mathf.Atan2(toVector2MP.y - centerCircle.y, toVector2MP.x - centerCircle.x))
                        *
                       Mathf.Rad2Deg;

        if (angle < 0)
        {
            angle += 360;
        }

        curMenuItem = (int)(angle / (360 / menuItems));

        if (curMenuItem != preMenuItem)
        {
            menuButtons[preMenuItem].sceneImage.color = menuButtons[preMenuItem].normalColor;
            preMenuItem = curMenuItem;
            menuButtons[curMenuItem].sceneImage.color = menuButtons[curMenuItem].highLightedColor;
        }
    }

    public void ButtonAction()
    {
        menuButtons[curMenuItem].sceneImage.color = menuButtons[curMenuItem].pressedColor;
        if (curMenuItem == 0)
        {
            print("Você pressionou o primeiro botão!");
        }
    }
}

[System.Serializable]
public class MenuButton
{
    public string name;
    public Image sceneImage;
    public Color normalColor = Color.white;
    public Color highLightedColor = Color.grey;
    public Color pressedColor = Color.gray;
}
